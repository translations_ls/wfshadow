# -*- coding: utf-8 -*-

"""
Module implementing dlgHideSegments.
"""

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog
from PyQt5.QtGui import QStandardItemModel

from Ui_dlg_hide_segments import Ui_dlgHideSegments

from dlg_progress_bar import dlgProgressBar

import segments_sorter as seg_sorter
from pathlib import Path

class dlgHideSegments(QDialog, Ui_dlgHideSegments):
    """
    Class documentation goes here.
    """
    def __init__(self, parent=None, folder: str='', wf_files: list=[], key='', engine='polyglot'):
        """
        Constructor
        
        @param parent reference to the parent widget
        @type QWidget
        """
        super(dlgHideSegments, self).__init__(parent)
        self.setupUi(self)
        
        # Initialization of main attributes
        self.folder = folder
        self.key = key
        self.engine = engine
        
        self.wf_files = wf_files
        self.sorted_segments = dict()
        
        # Setting models for the TableView and the ListView
        self.files_model = QStandardItemModel()
        self.sorted_model = QStandardItemModel()
        
        # Initial configurations of the models
        self.sorted_table_head = ['Language',
                                         'File name',
                                         'Id',
                                         'Source',
                                         'Score',
                                         'Seg Object']
        self.sorted_model.setColumnCount(len(self.sorted_table_head))
        self.files_model.setColumnCount(1)
        self.sorted_model.setHorizontalHeaderLabels(self.sorted_table_head)
        
        # Associating the models to their respective object (files list and sorted table)
        self.lvFiles.setModel(self.files_model)
        self.tvSortedFiles.setModel(self.sorted_model)
        self.tvSortedFiles.setColumnHidden(4, True)
        self.tvSortedFiles.setColumnHidden(5, True)
        
        # Defining the dimension of the table of Sorted files
        self.tvSortedFiles.resizeColumnToContents(1)
        self.tvSortedFiles.horizontalHeader().setStretchLastSection(True)
        
        # Filling the list of files with files
        self.__load_files()
    
    @pyqtSlot()
    def on_btDetLanguages_clicked(self):
        """
        Slot documentation goes here.
        """
        progress_bar = dlgProgressBar()
        segments = []
        for wf_file in self.wf_files:
            for par in wf_file.paragraphs:
                for seg in par.segments:
                    segments.append(seg)
        self.sorted_segments = seg_sorter.sort_segments(segments, self.key, self.engine, progress_bar)
        
        
        if self.sorted_model.rowCount() > 0:
            self.sorted_model.removeRows(0, self.sorted_model.rowCount())
        nrow = self.sorted_model.rowCount()
        for lang, segments in self.sorted_segments.items():
            for seg in segments:
                file_name = Path(seg[0].wf.wf_file_path).name
                self.sorted_model.insertRow(nrow)
                self.sorted_model.setData(self.sorted_model.index(nrow, 0),  lang)
                self.sorted_model.setData(self.sorted_model.index(nrow, 1),  file_name)
                self.sorted_model.setData(self.sorted_model.index(nrow, 2),  seg[0].seg_number)
                self.sorted_model.setData(self.sorted_model.index(nrow, 3),  seg[0].source.only_text)
                self.sorted_model.setData(self.sorted_model.index(nrow, 4),  seg[1])
                self.sorted_model.setData(self.sorted_model.index(nrow, 5),  seg[0])
                nrow += 1
        self.tvSortedFiles.resizeColumnToContents(1)
    
    @pyqtSlot()
    def on_btUpdateLang_clicked(self):
        """
        Slot documentation goes here.
        """
        selected_rows = self.tvSortedFiles.selectionModel().selectedRows()
        lang = self.lineEdit.text()
        for sel_row in selected_rows:
            sel_wf = self.sorted_model.index(sel_row.row(), 5).data()
            score = self.sorted_model.index(sel_row.row(), 4).data()
            original_lang = self.sorted_model.index(sel_row.row(), 0).data()
            edit_tuple = (sel_wf, score)
            if lang is not None:
                print(self.sorted_segments)
                self.sorted_segments.setdefault(lang,  [])
                self.sorted_segments[lang].append(edit_tuple)
                self.sorted_segments[original_lang].remove(edit_tuple)
                self.sorted_model.setData(self.sorted_model.index(sel_row.row(), 0), lang)
                print(self.sorted_segments)

    @pyqtSlot()
    def on_btHide_clicked(self):
        """
        Hide the segments and closes the dialog
        """
        progress_bar = dlgProgressBar()
        seg_sorter.hide_segments(self.sorted_segments, progress_bar)
        self.parent().close()
    
    @pyqtSlot()
    def on_btCancel_clicked(self):
        """
        Close the dialog
        """
        # To close the mdi SubWindow needs to make reference to its parent.
        # If only does self.close() it will close only the Widget(Dialog) without
        # closing its container
        self.parent().close()

    def __load_files(self):
        """
        """
        nrow = self.files_model.rowCount()
        for file in self.wf_files:
            file_name = Path(file.wf_file).name
            print(file_name)
            self.files_model.insertRow(nrow)
            self.files_model.setData(self.files_model.index(nrow, 0), str(file_name))
            nrow += 1
