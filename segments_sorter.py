__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2019. All rights are reserved.'
__license__ = 'GPL 3'

from pathlib import Path
from lang_detect.lang_detect import Lang
from wf_library.xml_wf import Wf
import pickle
from PyQt5.QtWidgets import QProgressBar

def save_key(key: str) -> None:
    """
    """
    file_path = Path('azure_key.pk').resolve()
    with open(file_path, 'wb') as fout:
        pickle.dump(key, fout, )

def load_key() -> str:
    """
    """
    file_path = Path('azure_key.pk').resolve()
    print(file_path.exists())
    if file_path.exists():
        with open(file_path, 'rb') as fin:
            return pickle.load(fin, encoding='utf-8')
    else:
        return ''

def browse_folder(folder: str, progress_bar: QProgressBar=None) -> list:
    """
    """
    folder_path = Path(folder)
    wf_files = []
    wf_extensions = ('txlf', 'txml')
    if folder_path.is_dir():
        act_file = 1
        total_files = len([file for ext in wf_extensions for file in folder_path.glob(f'.{ext}')])
        if progress_bar:
            progress_bar.setWindowTitle('Loading the WordFast files')
            for ext in wf_extensions:
                for f in folder_path.glob(f'*.{ext}'):
                    file_name = f.name
                    print(f)
                    progress_bar.update_prog_bar(act_file, total_files, f'Loading file {file_name}')
                    wf_files.append(Wf(str(f)))
                    act_file += 1
                    progress_bar.update_prog_bar(act_file, total_files, f'Loading file {file_name}')
        else:
            wf_files = [Wf(file) for ext in wf_extensions for file in folder_path.glob(f'.{ext}')]
        return wf_files
    else:
        return None

def sort_segments(segments: list, key: str='', engine: str='polyglot', progress_bar: QProgressBar=None) -> dict:
    """
    """
    languages = dict()
    max_files = len(segments)
    act_seg = 1
    progress_bar.setWindowTitle('Detecting the languages to sort the segments')
    for segment in segments:
        print(segment.wf.wf_file)
        file_name = Path(segment.wf.wf_file_path).name
        progress_bar.update_prog_bar(act_seg, max_files, f'Detecting the language of {segment.id} of '
                                                        f'file {file_name}')
        if segment.source.only_text:
            lang_obj = Lang(segment.source.all_text[:4999])
            lang_obj.detect_language(engine=engine, key=key)
            lang = lang_obj.language
            score = lang_obj.score
            seg_tuple = (segment, score)
            print(lang)
        else:
            lang = 'empty'
            seg_tuple =(segment, 0)
        languages.setdefault(lang,  [])
        languages[lang].append(seg_tuple)
        act_seg += 1
        progress_bar.update_prog_bar(act_seg, max_files, f'Detecting the language of {segment.id} of '
                                                        f'file {file_name}')
    return languages

def hide_segments(seg_dict: dict, progress_bar: QProgressBar=None) -> None:
    """
    Move the files to the root folder
    
    Arguments:
        seg_dict(dict): A dictionary with the list of segments that are part of a language.
    """
    total_segments = 0
    for lang, segments in seg_dict.items():
        print(segments)
        total_segments += len(seg_dict[lang])
    act_seg = 0
    progress_bar.setWindowTitle = 'Hiding the segments'
    for lang, segments in seg_dict.items():
        for seg_tuple in segments:
            
            segment = seg_tuple[0]
            file_name = Path(segment.wf.wf_file).name
            source_lang = segment.wf.source_lang
            print(source_lang[:2], lang)
            progress_bar.update_prog_bar(act_seg, total_segments, 
                                f'Hiding {segment.id} of {file_name}')
            if source_lang[:2].lower() != lang.lower():
                segment.hide()
            if source_lang[:2].lower() == lang.lower():
                segment.unhide()
            segment.wf.save_xml()
            progress_bar.update_prog_bar(act_seg, total_segments, 
                                f'Hiding {segment.id} of {file_name}')
            act_seg += 1

if __name__ == '__main__':
    folder = r'q:\zz_Temp\lsuau\Development\Python\pdf_language_sorter\files\sorted'
    files = browse_folder(folder)
    save_key(r'71b67cf13e9e466cb427f441e2e29e94')
    key = load_key()
    print(key)
    sorted_files = sort_segments(files, key, 'azure')
    
    
