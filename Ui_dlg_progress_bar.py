# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Q:\zz_Temp\lsuau\Development\Python\pdf_language_sorter\dlg_progress_bar.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_dlgProgressBar(object):
    def setupUi(self, dlgProgressBar):
        dlgProgressBar.setObjectName("dlgProgressBar")
        dlgProgressBar.resize(691, 58)
        dlgProgressBar.setSizeGripEnabled(True)
        self.verticalLayout = QtWidgets.QVBoxLayout(dlgProgressBar)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(dlgProgressBar)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.progressBar = QtWidgets.QProgressBar(dlgProgressBar)
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName("progressBar")
        self.verticalLayout.addWidget(self.progressBar)

        self.retranslateUi(dlgProgressBar)
        QtCore.QMetaObject.connectSlotsByName(dlgProgressBar)

    def retranslateUi(self, dlgProgressBar):
        _translate = QtCore.QCoreApplication.translate
        dlgProgressBar.setWindowTitle(_translate("dlgProgressBar", "Loading files"))
        self.label.setText(_translate("dlgProgressBar", "Loading files"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    dlgProgressBar = QtWidgets.QDialog()
    ui = Ui_dlgProgressBar()
    ui.setupUi(dlgProgressBar)
    dlgProgressBar.show()
    sys.exit(app.exec_())

