# -*- coding: utf-8 -*-

"""
Module implementing MainWindow.
"""

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QMainWindow, QFileDialog, QMdiSubWindow, qApp

from Ui_mw_main import Ui_MainWindow
from dlg_azure_key import dlgAzure
from dlg_hide_segments import dlgHideSegments
from dlg_progress_bar import dlgProgressBar
import segments_sorter as seg_sorter

from os.path import expanduser

class MainWindow(QMainWindow, Ui_MainWindow):
    """
    Class documentation goes here.
    """
    def __init__(self, parent=None):
        """
        Constructor
        
        @param parent reference to the parent widget
        @type QWidget
        """
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.azure_key = seg_sorter.load_key()
        self.engine = 'polyglot'
        self.dlg_sort_files = None
        if not self.azure_key:
            self.actionAzure_Engine.setDisabled(True)
        else:
            self.actionAzure_Engine.setEnabled(True)
        self.actionQuit.triggered.connect(qApp.quit)
    
    @pyqtSlot()
    def on_actionOpen_Folder_triggered(self):
        """
        Slot documentation goes here.
        """
        print('Opening the folder')
        folder_name = QFileDialog.getExistingDirectory(self,
                                                                     'Select the folder to process',
                                                                     expanduser('~'))
        progress_bar = dlgProgressBar()
        self.wf_files = seg_sorter.browse_folder(folder_name, progress_bar)
        print(self.wf_files)
        self.dlg_hide_segments = dlgHideSegments(parent=self,
                                                    folder=folder_name,
                                                    wf_files = self.wf_files, 
                                                    key=self.azure_key,
                                                    engine=self.engine)
        self.sw_hide_segments = self.create_subwindow(self.dlg_hide_segments)
        self.sw_hide_segments.show()
        
    
    @pyqtSlot()
    def on_actionAzureKey_triggered(self):
        """
        Slot documentation goes here.
        """
        self.dlg_azure = dlgAzure(parent=self, key=self.azure_key)
        self.dlg_azure.show()
        if self.dlg_azure.exec_():
            self.azure_key = self.dlg_azure.key
            print(self.azure_key)
    
    @pyqtSlot(bool)
    def on_actionAzure_Engine_triggered(self, checked):
        """
        Slot documentation goes here.
        
        @param checked DESCRIPTION
        @type bool
        """
        if checked:
            self.engine = 'azure'
            self.actionPolyglot.setChecked(False)
            self.actionLangid.setChecked(False)
            if self.dlg_hide_segments is not None:
                self.dlg_hide_segments.engine = self.engine
    
    @pyqtSlot(bool)
    def on_actionPolyglot_triggered(self, checked):
        """
        Slot documentation goes here.
        
        @param checked DESCRIPTION
        @type bool
        """
        if checked:
            self.engine = 'polyglot'
            self.actionAzure_Engine.setChecked(False)
            self.actionLangid.setChecked(False)
            if self.dlg_hide_segments is not None:
                self.dlg_hide_segments.engine = self.engine
    
    @pyqtSlot(bool)
    def on_actionLangid_triggered(self, checked):
        """
        Slot documentation goes here.
        
        @param checked DESCRIPTION
        @type bool
        """
        if checked:
            self.engine = 'langid'
            self.actionAzure_Engine.setChecked(False)
            self.actionPolyglot.setChecked(False)
            if self.dlg_hide_segments is not None:
                self.dlg_hide_segments.engine = self.engine

    def create_subwindow(self, dialog: object) -> None:
        """
        """
        subwindow = QMdiSubWindow()
        subwindow.setWidget(dialog)
        self.mdiArea.addSubWindow(subwindow)
        #self.mdiArea.setFixedSize(self.width() - 10, self.height() - 90)
        usable_height = self.height() - self.statusBar.height() - self.menuBar.height() - self.toolBar.height()
        print(f'{subwindow.height()} > {usable_height}')
        if subwindow.height() > usable_height :
            extra_widgets_height = self.statusBar.height() + self.menuBar.height() + self.toolBar.height()
            self.resize(self.width(), subwindow.height() + 20 + extra_widgets_height)
        if subwindow.width() > self.width():
            self.resize(self.height(), subwindow.width() + 10)
        return subwindow
