# -*- coding: utf-8 -*-

"""
Module implementing dlgAzure.
"""

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog

from Ui_dlg_azure_key import Ui_dlgAzure
from segments_sorter import save_key

class dlgAzure(QDialog, Ui_dlgAzure):
    """
    Class documentation goes here.
    """
    def __init__(self, parent=None, key: str=''):
        """
        Constructor
        
        @param parent reference to the parent widget
        @type QWidget
        """
        super(dlgAzure, self).__init__(parent)
        self.key = key
        self.setupUi(self)
        self.lineEditKey.setText(key)
    
    @pyqtSlot()
    def on_btOk_clicked(self):
        """
        Slot documentation goes here.
        """
        self.key = self.lineEditKey.text()
        save_key(self.key)
        self.accept()
    
    @pyqtSlot()
    def on_btCancel_clicked(self):
        """
        Slot documentation goes here.
        """
        self.reject()
