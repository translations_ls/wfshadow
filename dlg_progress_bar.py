# -*- coding: utf-8 -*-

"""
Module implementing dlgProgressBar.
"""
__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2019. All rights are reserved.'
__license__ = 'GPL 3'

from PyQt5.QtWidgets import QDialog, QApplication

from Ui_dlg_progress_bar import Ui_dlgProgressBar


class dlgProgressBar(QDialog, Ui_dlgProgressBar):
    """
    Class documentation goes here.
    """
    def __init__(self, parent=None):
        """
        Constructor
        
        @param parent reference to the parent widget
        @type QWidget
        """
        super(dlgProgressBar, self).__init__(parent)
        self.setupUi(self)
        self.show()
    
    def update_prog_bar(self, actual_prog: int, total_bar: int, file_name: str):
        """
        Update the progress bar
        Arguments:
        actual_prog: Actual progress of the bar
        total_bar: The total size of the values.
        file_name: File name of the file being processed.
        """
        print(actual_prog)
        print('Total: ',  total_bar)
        self.progressBar.setMinimum(0)
        self.progressBar.setValue(actual_prog)
        self.label.setText('{}'.format(str(file_name)))
        self.progressBar.setMaximum(total_bar)
        QApplication.processEvents()
