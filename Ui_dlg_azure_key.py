# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Q:\zz_Temp\lsuau\Development\Python\pdf_language_sorter\dlg_azure_key.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_dlgAzure(object):
    def setupUi(self, dlgAzure):
        dlgAzure.setObjectName("dlgAzure")
        dlgAzure.setWindowModality(QtCore.Qt.ApplicationModal)
        dlgAzure.resize(371, 128)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/Icons/icons/Oxygen-Icons.org-Oxygen-Actions-system-run.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        dlgAzure.setWindowIcon(icon)
        dlgAzure.setSizeGripEnabled(True)
        self.verticalLayout = QtWidgets.QVBoxLayout(dlgAzure)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(dlgAzure)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.lineEditKey = QtWidgets.QLineEdit(self.groupBox)
        self.lineEditKey.setMinimumSize(QtCore.QSize(311, 20))
        self.lineEditKey.setMaximumSize(QtCore.QSize(411, 20))
        self.lineEditKey.setEchoMode(QtWidgets.QLineEdit.PasswordEchoOnEdit)
        self.lineEditKey.setObjectName("lineEditKey")
        self.horizontalLayout.addWidget(self.lineEditKey)
        self.verticalLayout.addWidget(self.groupBox)
        self.frame = QtWidgets.QFrame(dlgAzure)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.frame)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.btOk = QtWidgets.QPushButton(self.frame)
        self.btOk.setMinimumSize(QtCore.QSize(75, 24))
        self.btOk.setMaximumSize(QtCore.QSize(75, 24))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/Icons/icons/Oxygen-Icons.org-Oxygen-Actions-dialog-ok-apply.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btOk.setIcon(icon1)
        self.btOk.setObjectName("btOk")
        self.horizontalLayout_2.addWidget(self.btOk)
        self.btCancel = QtWidgets.QPushButton(self.frame)
        self.btCancel.setMinimumSize(QtCore.QSize(75, 24))
        self.btCancel.setMaximumSize(QtCore.QSize(75, 24))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/Icons/icons/Oxygen-Icons.org-Oxygen-Actions-dialog-cancel.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btCancel.setIcon(icon2)
        self.btCancel.setObjectName("btCancel")
        self.horizontalLayout_2.addWidget(self.btCancel)
        self.verticalLayout.addWidget(self.frame)

        self.retranslateUi(dlgAzure)
        QtCore.QMetaObject.connectSlotsByName(dlgAzure)

    def retranslateUi(self, dlgAzure):
        _translate = QtCore.QCoreApplication.translate
        dlgAzure.setWindowTitle(_translate("dlgAzure", "Azure key"))
        dlgAzure.setToolTip(_translate("dlgAzure", "Edit or add your Azure key"))
        self.groupBox.setTitle(_translate("dlgAzure", "Azure key"))
        self.btOk.setText(_translate("dlgAzure", "Ok"))
        self.btCancel.setText(_translate("dlgAzure", "Cancel"))

from resources import images_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    dlgAzure = QtWidgets.QDialog()
    ui = Ui_dlgAzure()
    ui.setupUi(dlgAzure)
    dlgAzure.show()
    sys.exit(app.exec_())

